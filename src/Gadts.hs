{-# LANGUAGE GADTs #-}
{-# LANGUAGE KindSignatures #-} --needed fot type arithmetic
{-# LANGUAGE DataKinds #-}      --needed fot type arithmetic
-- {-# LANGUAGE PolyKinds #-}
{-# LANGUAGE TypeOperators #-}  --needed fot type arithmetic
-- check
-- http://mstksg.github.io/hmatrix/Numeric-LinearAlgebra-Static.html
-- for how to use GHC.TypeLits
-- Seems like there is some language extension missing to use tailN below
module Gadts where

import qualified GHC.TypeLits as TL --module implementing type arithmetic
--import Data.Proxy (Proxy(..))
-- |A redefinition of lists as GADT. Compare to the regular definition as
-- union type ie
-- data List content = Nil|Con content (List content)
-- Here the union type is generalised to a GADT and returns different types.
data List state content where
  Nil :: List Empty content
  Con :: content -> List state content -> List Populated content
-- |This needs datatypes to track the state of the list
data Empty
data Populated

-- |Point of this: checking applicability of @head@ during compile time
-- In contrast to the partial function @head@ for regular lists [], @headGadt@
-- is fully defined on its datatype.
headGadt :: List Populated a -> a
headGadt (Con x _) = x

-- |Problem: the tail function. Sometimes, the tail of a @List@ will be
-- populated, sometimes not. That's where inductive types come in handy
-- because the @state@ index of @List@ could be populated with an integer
-- instead of a type. Currently the only way to do that in Haskell is via
-- Presburger arithmetic at the type-level. See module GHC.TypeLits, which
-- actually implements type-level Peano arithmetic

--TODO: write lengthN to use the type parameter n to calculate length
--TODO: check the experimental static part of HMatrix to see how tailN
--could be made to work.
data ListN (n :: TL.Nat) content where
  NilN :: ListN 0 content
  ConN :: content -> ListN n content -> ListN  (n TL.+ 1) content
-- constraint KnownNat needed? No, KnownNat is needed only when trying
-- to get a value from the type, its function natSing returns an SNat,
-- ie natSing :: 2 should return a SNat 2 which can be pattern matched
-- against SNat x, this is implemented n natVal et al

-- |Again a version of a head that doesn't compile for empty lists because
-- n+1 cannot be unified with 0
headN :: ListN (n TL.+ 1) a -> a
headN (ConN x _)= x

-- |Another implementation of @head@, this time using type constraints
-- to exclude the empty list. This time EQ cannot be unified with GT
-- if n is zero
headN2 :: (TL.KnownNat n, TL.CmpNat n 0 ~ GT) => ListN n a -> a
headN2 (ConN x _)= x

-- |But tailN should work now, except if n=0 already
tailN :: (TL.KnownNat n, TL.KnownNat m, TL.CmpNat (n TL.- m) 0 ~ EQ) => ListN n a -> ListN m a
tailN = undefined
-- This should be
-- tailN (ConN _ xs)= xs
-- but the compiler doesn't now that n+1 ~ m+1 allows the conclusion
-- n~m because Nat is not defined recursively

-- tailN :: (TL.KnownNat n) => ListN n a -> ListN (n TL.- 1) a
-- tailN NilN = NilN
-- tailN (ConN _ xs) = xs
